<?php

class Home extends Controller{
    public function index() 
    {
        $data ['title'] = 'Home';
        $data['name'] = $this->model('HomeModel')->getUser();
        $data['users'] = $this->model('UserModel')->getAllUser();
        
        $this->view('templates/header', $data); 
        $this->view('home/index' ,$data);
        $this->view('templates/footer', $data);
    }


    public function about($company = 'SMKN 1')
    {
        $data['title'] = 'Home';
        $data['name'] = $this->model('HomeModel')->getAlluser();


        $data["judul"]="Abaout";
        $data["company"]= $company;
        $this->view('template/header', $data);
        $this->view('home/index',$data);
        $this->view('template/footer',$data);
    }    

    public function login()
    {
        $data['title'] = 'Login';
        $data['users'] = $this->model('UserModel');

        $this->view('register/index', $data);
    }
}
?>